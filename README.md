# Torrent CLI Downloader

[![Python 3.9](https://img.shields.io/badge/Python-3.9-blue)](https://www.python.org/downloads/release/python-312/) [![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT) [![Transmission](https://img.shields.io/badge/Transmission-CLI-blue)](https://transmissionbt.com/) [![Slugify](https://img.shields.io/badge/Slugify-Filename%20Sanitizer-green)](https://github.com/un33k/python-slugify) [![OS: Linux](https://img.shields.io/badge/OS-Linux-black)](https://www.linux.org/)

## Description

This script makes it easy for SSH users to automate the process of **downloading** a torrent, **moving** it to a public folder, **compressing** it, and **deleting** the original folder. It then **prints out the URL** to download the compressed file, making it easy to share with others.

## Requirements

* Python 3 (tested with Python 3.9)
* `transmission-cli` command-line tool

## Setup

**IMPORTANT:** This script only works in combination with the `transmission-cli` command as seen in this example (all paths and file names are examples):

```bash
transmission-cli --download-dir=/var/www/vhosts/yanawa.io/httpdocs/temp --finish=/root/scripts/torrent-finished.sh
```

* `--download-dir`: Where to save downloaded data.
* `--finish`: The **bash** script that will run once the download is finished.

Create a file named `script.sh` and add the following:

```bash
python /root/scripts/torrent-cli-downloader.py
```

This will tell Transmission to run the bash script, which in return launches the Python script. You can also use it as an alias:

```bash
alias torrent="transmission-cli --download-dir=/var/www/vhosts/yanawa.io/httpdocs/temp --finish=/root/scripts/torrent-finished.sh "
```

### 1. Installation

On Debian and similar Linux distributions, install the required `transmission-cli` tool via `sudo apt-get install transmission-cli` or on macOS via [Homebrew](https://brew.sh/) `brew install transmission-cli`.

### 2. Configuration

**Before running this script** in your terminal as `python torrent-cli-downloader.py`, make sure to adjust the following constants on lines `8` and `9` in the script file:

* `DOWNLOAD_DIRECTORY_URL`: Base URL the final filename is being added to
* `TRANSMISSIONS_DIRECTORY`: Path to the transmissions directory (default: `~/.config/transmission`)

## Usage

1. Open the script file and configure the above-mentioned constants.
2. Adjust the addition flags in the following section *(optional)*.
3. Use the `python` or `python3` command to run the `.py` file.

```bash
python torrent-cli-downloader.py
```

## Flags

The script uses the following flags to control its behavior:

* `COMPRESS_DOWNLOAD`: Compress the downloaded file(s) (default: `True`)
* `SANITIZE_FILE_NAME`: Sanitize the filename by removing special characters (default: `True`)
* `DELETE_AFTER_COMPRESSION`: Delete the original file(s) after compression (default: `True`)

## Notes

* The script assumes that the `transmission-cli` command-line tool is installed and available. You can download it from the [Transmission Project](https://transmissionbt.com/) website.
* The script uses the `slugify` library to sanitize the filename. You can learn more about slugify on its [GitHub page](https://github.com/un33k/python-slugify).
* The script uses the `subprocess` library to run external commands.
* The script uses the `shutil` library to move and delete files

## Authors

* **Kolja Nolte** <[kolja.nolte@gmail.com](mailto:kolja.nolte@gmail.com)>

## License

This script is licensed under the **MIT License**, which permits the free use, modification, and distribution of software without warranty or liability. Read the full license in the [LICENSE](https://gitlab.com/thaikolja/torrent-cli-downloader/-/blob/main/LICENSE?ref_type=heads) file.
