import os
import subprocess
import slugify
import shutil
import pathlib

# Define constants
DOWNLOAD_DIRECTORY_URL = 'https://yanawa.io/temp'  # URL for the download directory
TRANSMISSIONS_DIRECTORY = '/root/.config/transmission'  # Path to the transmissions directory

# Flags for controlling the behavior of the script
COMPRESS_DOWNLOAD = True  # Flag to determine if the download should be compressed
SANITIZE_FILE_NAME = True  # Flag to determine if the filename should be sanitized
DELETE_AFTER_COMPRESSION = True  # Flag to determine if the file should be deleted after compression


def shutdown(message: str, is_exit: bool = True):
    """
    Function to shutdown the script and print an error message.

    Args:
        message (str): The error message to print.
        is_exit (bool, optional): Flag to determine if the script should exit. Defaults to True.
    """

    # Kill the Transmission process
    subprocess.call(['killall', 'transmission-cli'])

    # Print the error message and exit the script
    if is_exit:
        exit(message)
    # Just print a message
    else:
        print(message)


# Check if the necessary environment variable `TR_TORRENT_NAME` is set
if not (torrent_name := os.environ.get('TR_TORRENT_NAME')):
    shutdown('ERROR: TR_TORRENT_NAME environment variable is not set. Was the download successful?')

# Check if the necessary environment variable `TR_TORRENT_DIR` is set
if not (torrent_dir := os.environ.get('TR_TORRENT_DIR')):
    shutdown('ERROR: TR_TORRENT_DIR environment variable is not set. Was the download successful?')

# Retrieve the name of the torrent file and the download directory from the environment variables
torrent_name = os.environ['TR_TORRENT_NAME']

# Convert the download directory to a pathlib.Path object
torrent_dir = pathlib.Path(os.environ['TR_TORRENT_DIR'])

# Sanitize the filename by removing special characters if the SANITIZE_FILE_NAME flag is set
sanitized_file_name = slugify.slugify(torrent_name) if SANITIZE_FILE_NAME else torrent_name

# Construct the full path of the downloaded file
downloaded_file_path = torrent_dir / torrent_name

# Check if the downloaded file is a directory
if downloaded_file_path.is_dir():
    # If it is a directory, zip the directory
    zip_file_path = torrent_dir / f"{sanitized_file_name}.zip"
    subprocess.run(['clear'])

    # Zip the downloaded file
    subprocess.run(['zip', '-r', zip_file_path, downloaded_file_path])

    # Check if the zipped file exists
    if not zip_file_path.exists():
        shutdown('ERROR: The downloaded torrent file(s) seem to be missing.')

    # Generate the download URL
    url = f"{DOWNLOAD_DIRECTORY_URL}/{sanitized_file_name}.zip"

    # Clean up the downloaded file if the DELETE_AFTER_COMPRESSION flag is set
    if DELETE_AFTER_COMPRESSION:
        shutil.rmtree(downloaded_file_path)

    # Clear the terminal
    subprocess.run(['clear'])

    # Shutdown the torrent process
    shutdown(url, False)
else:
    # If the downloaded file is not a directory, check if the file exists
    shutdown(f"ERROR: The downloaded file(s) could not be found in {torrent_dir}.")

# Clean up saved .torrent files
shutil.rmtree(f"{TRANSMISSIONS_DIRECTORY}/torrents")

# Clean up Transmission resume files
shutil.rmtree(f"{TRANSMISSIONS_DIRECTORY}/resume")
